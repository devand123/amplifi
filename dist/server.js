process.env.PWD = process.cwd();

var express = require('express'),
		app = express(),

		// set environment variables
		prod = process.env.PWD,
		dev = __dirname + '/app',
		env_dir = app.get('env') === 'production' ? prod : dev;

app.set('port', process.env.PORT || 3000);

app.configure(function() {
	app.use(express.static(env_dir));
});

app.get('/api/genre', function(req, res) {
	var genres = {
		success: true,
		genre_type: ['all', 'rock', 'americana', 'othergenre']
	};
	res.json(genres);
});

app.get('/api/all_artists/:limit?/:flag?', function(req, res) {
	var sample_artists = {
		success: true,
		genre: 'all',
		artist: [
			{
				user_name: 'jeneesusername',
				display_name: 'Jenee Halstead',
				route: 'http://routetotheirpage.com',
				banner_image: 'images/Jenee_banner.jpg',
				square_image: 'images/small/Jenee.jpg',
				largesquare_image: 'images/square/Jenee.jpg',
				rectangle_image: 'images/rectangle/Jenee.jpg',
				genre: 'rock',
				display_type: 'featured',
				is_new: false,
				position: 2
			},
			{
				user_name: 'kingsleyflood',
				display_name: 'Kingsley Flood',
				route: 'http://routetotheirpage.com',
				banner_image: 'images/Jenee_banner.jpg',
				square_image: 'images/small/KingsleyFlood.jpg',
				largesquare_image: 'images/square/KingsleyFlood.jpg',
				rectangle_image: 'images/rectangle/KingsleyFlood.jpg',
				genre: 'rock',
				display_type: 'largesquare',
				is_new: true,
				position: 5
			},
			{
				user_name: 'atc',
				display_name: 'Air Traffic Controller',
				route: 'http://routetotheirpage.com',
				banner_image: 'images/AirTrafficController.jpg',
				square_image: 'images/small/AirTrafficController.jpg',
				largesquare_image: 'images/square/AirTrafficController.jpg',
				rectangle_image: 'images/rectangle/AirTrafficController.jpg',
				genre: 'americana',
				display_type: 'rectangle',
				is_new: true,
				position: null
			},
			{
				user_name: 'ep',
				display_name: 'Ellis Paul',
				route: 'http://routetotheirpage.com',
				banner_image: 'images/EllisPaul.jpg',
				square_image: 'images/small/EllisPaul.jpg',
				largesquare_image: 'images/square/EllisPaul.jpg',
				rectangle_image: 'images/rectangle/EllisPaul.jpg',
				genre: 'americana',
				display_type: 'square',
				is_new: false,
				position: null
			},
			{
				user_name: 'ab',
				display_name: 'Amy Black',
				route: 'http://routetotheirpage.com',
				banner_image: 'images/AmyBlack.jpg',
				square_image: 'images/small/AmyBlack.jpg',
				largesquare_image: 'images/square/AmyBlack.jpg',
				rectangle_image: 'images/rectangle/AmyBlack.jpg',
				genre: 'americana',
				display_type: 'square',
				is_new: false,
				position: null
			},
			{
				user_name: 'ls',
				display_name: 'Los Straight',
				route: 'http://routetotheirpage.com',
				banner_image: 'images/losstraight.jpg',
				square_image: 'images/small/losstraight.jpg',
				largesquare_image: 'images/square/losstraight.jpg',
				rectangle_image: 'images/rectangle/losstraight.jpg',
				genre: 'americana',
				display_type: 'largesquare',
				is_new: false,
				position: 9
			},
			{
				user_name: 'atc',
				display_name: 'Air Traffic Controller',
				route: 'http://routetotheirpage.com',
				banner_image: 'images/AirTrafficController.jpg',
				square_image: 'images/small/AirTrafficController.jpg',
				largesquare_image: 'images/square/AirTrafficController.jpg',
				rectangle_image: 'images/rectangle/AirTrafficController.jpg',
				genre: 'americana',
				display_type: 'largesquare',
				is_new: true,
				position: null
			},
			{
				user_name: 'ep',
				display_name: 'Ellis Paul',
				route: 'http://routetotheirpage.com',
				banner_image: 'images/EllisPaul.jpg',
				square_image: 'images/small/EllisPaul.jpg',
				largesquare_image: 'images/square/EllisPaul.jpg',
				rectangle_image: 'images/rectangle/EllisPaul.jpg',
				genre: 'americana',
				display_type: 'square',
				is_new: false,
				position: null
			},
			{
				user_name: 'kingsleyflood',
				display_name: 'Kingsley Flood',
				route: 'http://routetotheirpage.com',
				banner_image: 'images/Jenee_banner.jpg',
				square_image: 'images/small/KingsleyFlood.jpg',
				largesquare_image: 'images/square/KingsleyFlood.jpg',
				rectangle_image: 'images/rectangle/KingsleyFlood.jpg',
				genre: 'rock',
				display_type: 'square',
				is_new: true,
				position: 5
			},
			{
				user_name: 'ab',
				display_name: 'Amy Black',
				route: 'http://routetotheirpage.com',
				banner_image: 'images/AmyBlack.jpg',
				square_image: 'images/small/AmyBlack.jpg',
				largesquare_image: 'images/square/AmyBlack.jpg',
				rectangle_image: 'images/rectangle/AmyBlack.jpg',
				genre: 'americana',
				display_type: 'rectangle',
				is_new: true,
				position: null
			},
			{
				user_name: 'ep',
				display_name: 'Ellis Paul',
				route: 'http://routetotheirpage.com',
				banner_image: 'images/EllisPaul.jpg',
				square_image: 'images/small/EllisPaul.jpg',
				largesquare_image: 'images/square/EllisPaul.jpg',
				rectangle_image: 'images/rectangle/EllisPaul.jpg',
				genre: 'americana',
				display_type: 'square',
				is_new: false,
				position: null
			},
			{
				user_name: 'ab',
				display_name: 'Amy Black',
				route: 'http://routetotheirpage.com',
				banner_image: 'images/AmyBlack.jpg',
				square_image: 'images/small/AmyBlack.jpg',
				largesquare_image: 'images/square/AmyBlack.jpg',
				rectangle_image: 'images/rectangle/AmyBlack.jpg',
				genre: 'americana',
				display_type: 'square',
				is_new: false,
				position: null
			}
		]
	};
	res.json(sample_artists);
});

app.get('/api/artist/:genre_type/:limit?/:flag?', function(req, res) {
	var sample_artists = [
	//othergenre
	{
		success: true,
		genre: 'othergenre',
		artist: [
			{
				user_name: 'ep',
				display_name: 'Ellis Paul',
				route: 'http://routetotheirpage.com',
				banner_image: 'images/EllisPaul.jpg',
				square_image: 'images/small/EllisPaul.jpg',
				largesquare_image: 'images/square/EllisPaul.jpg',
				rectangle_image: 'images/rectangle/EllisPaul.jpg',
				genre: 'othergenre',
				display_type: 'square',
				is_new: false,
				position: null
			},
			{
				user_name: 'ab',
				display_name: 'Amy Black',
				route: 'http://routetotheirpage.com',
				banner_image: 'images/AmyBlack.jpg',
				square_image: 'images/small/AmyBlack.jpg',
				largesquare_image: 'images/square/AmyBlack.jpg',
				rectangle_image: 'images/rectangle/AmyBlack.jpg',
				genre: 'othergenre',
				display_type: 'square',
				is_new: false,
				position: null
			}
		]
	},
	// rock
	{
		success: true,
		genre: 'rock',
		artist: [
			{
				user_name: 'kingsleyflood',
				display_name: 'Kingsley Flood',
				route: 'http://routetotheirpage.com',
				banner_image: 'images/Jenee_banner.jpg',
				square_image: 'images/small/KingsleyFlood.jpg',
				largesquare_image: 'images/square/KingsleyFlood.jpg',
				rectangle_image: 'images/rectangle/KingsleyFlood.jpg',
				genre: 'rock',
				display_type: 'square',
				is_new: true,
				position: 5
			},
			{
				user_name: 'jeneesusername',
				display_name: 'Jenee Halstead',
				route: 'http://routetotheirpage.com',
				banner_image: 'images/Jenee_banner.jpg',
				square_image: 'images/small/Jenee.jpg',
				largesquare_image: 'images/square/Jenee.jpg',
				rectangle_image: 'images/rectangle/Jenee.jpg',
				genre: 'rock',
				display_type: 'square',
				is_new: false,
				position: 2
			},
			{
				user_name: 'kingsleyflood',
				display_name: 'Kingsley Flood',
				route: 'http://routetotheirpage.com',
				banner_image: 'images/Jenee_banner.jpg',
				square_image: 'images/small/KingsleyFlood.jpg',
				largesquare_image: 'images/square/KingsleyFlood.jpg',
				rectangle_image: 'images/rectangle/KingsleyFlood.jpg',
				genre: 'rock',
				display_type: 'largesquare',
				is_new: true,
				position: 5
			},
			{
				user_name: 'kingsleyflood',
				display_name: 'Kingsley Flood',
				route: 'http://routetotheirpage.com',
				banner_image: 'images/Jenee_banner.jpg',
				square_image: 'images/small/KingsleyFlood.jpg',
				largesquare_image: 'images/square/KingsleyFlood.jpg',
				rectangle_image: 'images/rectangle/KingsleyFlood.jpg',
				genre: 'rock',
				display_type: 'square',
				is_new: true,
				position: 5
			},
			{
				user_name: 'jeneesusername',
				display_name: 'Jenee Halstead',
				route: 'http://routetotheirpage.com',
				banner_image: 'images/Jenee_banner.jpg',
				square_image: 'images/small/Jenee.jpg',
				largesquare_image: 'images/square/Jenee.jpg',
				rectangle_image: 'images/rectangle/Jenee.jpg',
				genre: 'rock',
				display_type: 'square',
				is_new: false,
				position: 2
			},
			{
				user_name: 'kingsleyflood',
				display_name: 'Kingsley Flood',
				route: 'http://routetotheirpage.com',
				banner_image: 'images/Jenee_banner.jpg',
				square_image: 'images/small/KingsleyFlood.jpg',
				largesquare_image: 'images/square/KingsleyFlood.jpg',
				rectangle_image: 'images/rectangle/KingsleyFlood.jpg',
				genre: 'rock',
				display_type: 'largesquare',
				is_new: true,
				position: 5
			},
			{
				user_name: 'kingsleyflood',
				display_name: 'Kingsley Flood',
				route: 'http://routetotheirpage.com',
				banner_image: 'images/Jenee_banner.jpg',
				square_image: 'images/small/KingsleyFlood.jpg',
				largesquare_image: 'images/square/KingsleyFlood.jpg',
				rectangle_image: 'images/rectangle/KingsleyFlood.jpg',
				genre: 'rock',
				display_type: 'square',
				is_new: true,
				position: 5
			},
			{
				user_name: 'jeneesusername',
				display_name: 'Jenee Halstead',
				route: 'http://routetotheirpage.com',
				banner_image: 'images/Jenee_banner.jpg',
				square_image: 'images/small/Jenee.jpg',
				largesquare_image: 'images/square/Jenee.jpg',
				rectangle_image: 'images/rectangle/Jenee.jpg',
				genre: 'rock',
				display_type: 'square',
				is_new: false,
				position: 2
			},
			{
				user_name: 'kingsleyflood',
				display_name: 'Kingsley Flood',
				route: 'http://routetotheirpage.com',
				banner_image: 'images/Jenee_banner.jpg',
				square_image: 'images/small/KingsleyFlood.jpg',
				largesquare_image: 'images/square/KingsleyFlood.jpg',
				rectangle_image: 'images/rectangle/KingsleyFlood.jpg',
				genre: 'rock',
				display_type: 'largesquare',
				is_new: true,
				position: 5
			}
		]
	},
	// americana
	{
		success: true,
		genre: 'americana',
		artist: [
			{
				user_name: 'ab',
				display_name: 'Amy Black',
				route: 'http://routetotheirpage.com',
				banner_image: 'images/AmyBlack.jpg',
				square_image: 'images/small/AmyBlack.jpg',
				largesquare_image: 'images/square/AmyBlack.jpg',
				rectangle_image: 'images/rectangle/AmyBlack.jpg',
				genre: 'americana',
				display_type: 'square',
				is_new: false,
				position: null
			},
			{
				user_name: 'ep',
				display_name: 'Ellis Paul',
				route: 'http://routetotheirpage.com',
				banner_image: 'images/EllisPaul.jpg',
				square_image: 'images/small/EllisPaul.jpg',
				largesquare_image: 'images/square/EllisPaul.jpg',
				rectangle_image: 'images/rectangle/EllisPaul.jpg',
				genre: 'americana',
				display_type: 'square',
				is_new: false,
				position: null
			},
			{
				user_name: 'ls',
				display_name: 'Los Straight',
				route: 'http://routetotheirpage.com',
				banner_image: 'images/losstraight.jpg',
				square_image: 'images/small/losstraight.jpg',
				largesquare_image: 'images/square/losstraight.jpg',
				rectangle_image: 'images/rectangle/losstraight.jpg',
				genre: 'americana',
				display_type: 'largesquare',
				is_new: false,
				position: 9
			},
			{
				user_name: 'atc',
				display_name: 'Air Traffic Controller',
				route: 'http://routetotheirpage.com',
				banner_image: 'images/AirTrafficController.jpg',
				square_image: 'images/small/AirTrafficController.jpg',
				largesquare_image: 'images/square/AirTrafficController.jpg',
				rectangle_image: 'images/rectangle/AirTrafficController.jpg',
				genre: 'americana',
				display_type: 'largesquare',
				is_new: true,
				position: null
			},
			{
				user_name: 'ab',
				display_name: 'Amy Black',
				route: 'http://routetotheirpage.com',
				banner_image: 'images/AmyBlack.jpg',
				square_image: 'images/small/AmyBlack.jpg',
				largesquare_image: 'images/square/AmyBlack.jpg',
				rectangle_image: 'images/rectangle/AmyBlack.jpg',
				genre: 'americana',
				display_type: 'square',
				is_new: false,
				position: null
			},
			{
				user_name: 'ep',
				display_name: 'Ellis Paul',
				route: 'http://routetotheirpage.com',
				banner_image: 'images/EllisPaul.jpg',
				square_image: 'images/small/EllisPaul.jpg',
				largesquare_image: 'images/square/EllisPaul.jpg',
				rectangle_image: 'images/rectangle/EllisPaul.jpg',
				genre: 'americana',
				display_type: 'square',
				is_new: false,
				position: null
			},
			{
				user_name: 'ls',
				display_name: 'Los Straight',
				route: 'http://routetotheirpage.com',
				banner_image: 'images/losstraight.jpg',
				square_image: 'images/small/losstraight.jpg',
				largesquare_image: 'images/square/losstraight.jpg',
				rectangle_image: 'images/rectangle/losstraight.jpg',
				genre: 'americana',
				display_type: 'largesquare',
				is_new: false,
				position: 9
			},
			{
				user_name: 'atc',
				display_name: 'Air Traffic Controller',
				route: 'http://routetotheirpage.com',
				banner_image: 'images/AirTrafficController.jpg',
				square_image: 'images/small/AirTrafficController.jpg',
				largesquare_image: 'images/square/AirTrafficController.jpg',
				rectangle_image: 'images/rectangle/AirTrafficController.jpg',
				genre: 'americana',
				display_type: 'largesquare',
				is_new: true,
				position: null
			}
		]
	}
	];

	var i, len = sample_artists.length;
	for(i = 0; i < len; i++) {
		if(req.params.genre_type === sample_artists[i].genre) {
			res.json(sample_artists[i]);
			console.log(sample_artists[i]);
		}
	}
});

app.listen(app.get('port'));
console.log('Listening on port ' + app.get('port'), 'in ' + app.get('env') + ' mode');

