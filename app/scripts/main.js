require.config({
  paths: {
      jquery: 'vendor/jquery.min',
      underscore: 'vendor/underscore.min',
      backbone: 'vendor/backbone.min',
      ejs: 'vendor/ejs',
      isotope: 'vendor/jquery.isotope.min',
      perfectMasonry: 'vendor/jquery.isotope.perfectmasonry',
      viewport: 'vendor/jquery.viewport.min',
      inf_scroll: 'vendor/jquery.infinitescroll',
      bootstrap: 'vendor/bootstrap'
  },
  shim: {
    bootstrap: {
      deps: ['jquery'],
      exports: 'jquery'
    },
    ejs: {
      exports: 'EJS'
    },
    isotope: ['jquery'],
    perfectMasonry: ['jquery', 'isotope'],
    viewport: ['jquery'],
    inf_scroll: ['jquery'],
    underscore: {
      exports: '_'
    },
    backbone: {
      deps: ['jquery', 'underscore'],
      exports: 'Backbone'
    }
  }
});

require([
  'app',
  'utils/overlay',
  'utils/activate_header',
  'da_isotope'
], function (App, overlay, activate_header, isotope) {
  'use strict';

  App.init(function() {

    var container = $('#container'),
        filters = $('#filters'),
        header = $('#header');

    // store a reference to our container in App
    App.container_el = container;

    overlay.start();
    $('body').removeClass('invisible');

    // Activate the header and all of it's methods
    activate_header(header);

    // Initialize the isotope container
    isotope.init(container, function() {
      container.parent().removeClass('invisible');
      overlay.finish();
      
      // Initialize the filters
      isotope.filters_init(filters);
    });

  });

});