define(['app'], function(App) {
	
	// Append all events that the header uses
	var activate_header = function(header) {
		header
      .on('click', ' .responsive-nav, .search-contain > a', function(e) {
        e.preventDefault();
        var self = $(this),
            target = $(e.target),
            nav_contain = header.find('#nav-contain'),
            search_contain = header.find('.search-contain'),
            el = self.parents('.search-contain').length > 0 ? search_contain : nav_contain; 
        
        $('.expanded').removeClass('expanded');
        el.toggleClass('expanded', !el.hasClass('expanded'));

        $(window)
          .off('resize')
          .on('resize', function(evt) {
            el.removeClass('expanded');
          });

        setTimeout(function() {
          $('body').on('click', function(evt) {
            var target2 = $(evt.target);

            if(target2.parents('#nav-contain').length > 0 ||
               target2.parents('.search-contain').length > 0) return;

            el.removeClass('expanded');
            $(this).off('click');
          });
        }, 100);

      })
      // search form submission handling
      .on('click', '#search a', function(e) {
        e.preventDefault();
        var form = $(this).parent(),
            input = form.find('input');

        if(!input.val().length) return;
        form.trigger('submit');
      });
	};

	return activate_header;

});