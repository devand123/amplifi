define(['app'], function(App) {
	'use strict';

	var overlay = {
		el: $('<div>', {
					'class': 'overlay'
				}),
		loading: function() {
			 return $('<img>', {
								'src': 'images/utils/ajax-loader.gif'
							});
		},
		error: function(msg) {
			msg = msg || 'An error has occured.';

			var error_div = $('<div>', {
												'class': 'error-message'
											})
											.html('<h1>Error</h1><p>' + msg + '</p>')
											.append($('<span>', { 'class': 'close', 'text': 'X' }))
											.on('click', '.close', function(e) {
												overlay.finish();
											});

			error_div
				.off('click')
				.on('click', '.close', function(evt) {
					overlay.finish();
				});

			return error_div;
		},
		start: function(type, msg, prependTo) {
			type = type ? type: 'loading';
			prependTo = prependTo || 'body';

			this.el
				.html(this[type](msg))
				.prependTo(prependTo)
				.show();

			if(prependTo === 'body') {
				$('body').addClass('no-scroll');
			}
		},
		finish: function() {
			this.el.fadeOut(500, function() {
				$(this).remove();
				$('body').removeClass('no-scroll');
			});
		}
	};

	return overlay;

});