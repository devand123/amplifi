define(['app'], function() {

	var template_methods = {
		capitalize: function(string) {
			return string.charAt(0).toUpperCase() + string.slice(1);
		}
	};
	
	var make_template = function(file, data) {
		data = $.extend({}, data, template_methods);

		var template = new EJS({
			url: '../../templates/' + file
		}).render(data);

		return template;
	};

	return make_template;

});