define(['app', 'utils/overlay'], function(App, overlay) {

	var loading_div,
			load_more_error = function(msg, fn) {
				console.log('load more error');
				loading_div = loading_div || $('<div>')
																			.addClass('loading-msg')
																			.html(msg);

				loading_div.appendTo('body').fadeIn('slow');
				
				// optional callback function
				if(fn && _.isFunction(fn)) {
					fn();
				}

				setTimeout(function() {
					loading_div.fadeOut('slow', function() {
						$(this).remove();
					});
				}, 2000);
			};

	return load_more_error;

});