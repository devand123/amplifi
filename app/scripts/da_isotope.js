// ALL DISCOVER ARTISTS ISOTOPE FUNCTIONALITY, AND METHODS DEFINED HERE
define([
	'app',
	'api',
	'utils/overlay',
	'utils/maketemplate',
	'utils/load_more_error',
	// jquery plugins
	'isotope',
	'perfectMasonry',
	'inf_scroll'
], function(App, API, overlay, maketemplate, load_more_error) {
	'use strict';
	
	var isotope = {};

	isotope.init = function(el, callback) {
		//el  = container

		API.get_all_artists({
			flag: 1
		}, function(data) {

			isotope.position(data, function(positioned_artitsts) {
				// overwrite data.artist with the returned appropriately
				// positioned artists list
				data.artist = positioned_artitsts;

				var html = maketemplate('artists.ejs', data);
				el.html(html);

				// Once images are loaded
				el.imagesLoaded(function() {

					// Initialize isotope on the container
					el.isotope({
			      itemSelector: '.item',
			      animationEngine: 'best-available',
			      layoutMode: 'perfectMasonry',
			      perfectMasonry: {
			        columnWidth: 40,
			        rowHeight: 55,
			        layout: 'vertical'
			      }
			    });

					// infinite scroll initialize
			    isotope.infscroll_init(el);

			    // init callback
					if(callback && _.isFunction(callback)) {
						setTimeout(function() {
							callback();
						}, 500);
					}

			  });

			});

		});

	}; // end isotope.init

	isotope.infscroll_init = function(el) {
		// start infinitescroll
    el.infinitescroll({
    	animate: false,
    	appendCallback: false,
    	dataType: 'json',
    	customAPI: function(fn) {
    		retrieve_and_build_artists(fn);
    	},
    	behavior: 'amplifi',
    	loading: {
    		selector: $('.loading-msg'),
    		img: 'images/utils/ajax-loader-light.gif',
    		msgText: 'Loading more artists...',
    		finishedMsg: 'No more artists available.',
    		speed: 'slow',
    		finishedCallback: function() {
    			var self = this;
    			// altered the plugin to include this throttle
    			// setup a throttle so that it doesn't load too many times at once
    			self.throttle = true;

    			// reset the throttle after half a second
    			_.delay(function() {
    				self.throttle = false;
    				isotope.utils.remove_invisiblity();
    			}, 1000);
    		}
    	}
    }); // no callback needed as we call customAPI above :)

	}; // end isotope.infscroll_init

	isotope.position = function(data, callback) {

		var positionable = [],
				other = [];

		// put all artists w/ a position attribute in the positionable array
		// and all others w/out position in the other array
		_.each(data.artist, function(artist, i) {
			if(artist.position) {
				positionable.push(artist);
			} else {
				other.push(artist);
			}
		});

		// sort and rewrite the positionable array in order
		positionable = _.sortBy(positionable, function(pos) {
			return pos.position;
		});

		// build out the overall sorted/positioned list of artists
		_.each(positionable, function(pos) {
			var position = pos.position - 1; // -1 to match the actual array index
			
			if(other[position]) {
				other.splice(position, 0, pos);
			} else {
				other[position] = pos;
			}
		});

		callback(other);

	}; // end isotope.position_data

	/* DEPRECATED POSITION. LEAVING HERE FOR NOW
	isotope.deprecated_position = function(container, callback) {

		var items = container.find('.item'),
				positionable = [],
				all_els = [];

		items.each(function(i) {
			var self = $(this),
					position = self.data('position');

			if(position) {
				positionable.push(self);
			} else {
				all_els.push(self);
			}

		});

		// overwrite positionable with an ordered version
		// from smallest position to largest
		positionable = _.sortBy(positionable, function(pos) {
			return $(pos).data('position');
		});

		// put the positionable items in with the rest of all the
		// other items in their appropriate positions
		_.each(positionable, function(p_el, i) {
				var position = $(p_el).data('position') - 1;

				if(all_els[position]) {
					all_els.splice(position, 0, p_el);
				} else {
					all_els[position] = p_el;
				}

		});

		// clear the containers rendered html
		container.html('');

		// append the new set of elements in their new order
		$.each(all_els, function(i, el) {
			container.append(el);
		});

		if(callback && _.isFunction(callback)) {
			callback();
		}

	}; // end isotope.deprecated_position
	*/

	isotope.filters_collection = [];

	isotope.filters = function(method, filter, callback) {

		filter = filter || '*';

		var filters = isotope.filters_collection,
				methods = {
					has: function() {
						return _.contains(filters, filter);
					},
					reset: function() {
						// used for *
						filters = isotope.filters_collection = [];
						filters.push(filter);
					},
					add: function() {
						var idx = filters.indexOf(filter);
						if(idx === -1) {
							filters.push(filter);
						}
					},
					remove: function() {
						var idx = filters.indexOf(filter);
						if(idx > -1) {
							filters.splice(idx, 1);
						}
					}
				};

		if(method) {
			methods[method]();
		}
		if(callback && _.isFunction(callback)) {
			callback();
		}

		return filters;

	}; // end isotope.filters

	isotope.filters_init = function(el) {
		// Build the list of genre filters
		API.get_genres(function(data) {
			var html = maketemplate('filters.ejs', data);
			el.html(html);
			build_fixed_filters(html);
		});

		// doesnt really need its own method as its reused
		function build_fixed_filters(html) {
			var filter_offset = el.offset().top + el.height(),
					body = $('body'),
					filters_fixed = $('.filters_fixed')
															.find('ul').html(html)
														.end();

			$(window).on('scroll', function(evt) {
				var body_offset = body.scrollTop() || $(document).scrollTop();
				
				if(body_offset > filter_offset) {
					filters_fixed.removeClass('hidden');
				} else {
					filters_fixed.addClass('hidden');
				}

			});
		}

		// Initially put it in the '*' filter
		isotope.filters('reset', '*');

		$('#filters, .filters_fixed')
			.on('click', 'a', function(e) {
				e.preventDefault();

				var filter = $(this).data('filter'),
						self = $('a[data-filter="' + filter + '"]'),
						filters = self.parent().siblings(),
						container = $('#container');

				self.toggleClass('selected', !self.hasClass('selected'));
				if(self.is('[data-filter="*"]')) self.addClass('selected');

				if(!filter) { return; }

				if(self.is('.selected')) {

					if(filter === '*') {

						isotope.filters('reset', '*', function() {
							filters.find('a').removeClass('selected');
						});

					} else {

						filters.find('[data-filter="*"]').removeClass('selected');

						isotope.filters('remove', '*');
						isotope.filters('add', filter);

					}
				} else {

					isotope.filters('remove', filter);

					if(!filters.find(' .selected').length) {
						isotope.filters('reset', '*', function() {
							filters.find(' a[data-filter="*"]').addClass('selected');
						});
					}

				}

				isotope.filter(container, isotope.filters());

			});

	}; // end isotop.filters_init

	isotope.filters_finished = [];

	isotope.filter = function(el, filters) {

		el.infinitescroll('resume');

		var	classify = function(filters) {
			var classified_filters = [];
			_.each(filters, function(filter) {
				filter = filter === '*' ? '*' : '.' + filter;
				classified_filters.push(filter);
			});
			return classified_filters;
		};

		try {
			el.isotope({ filter: classify(filters).join(', ') });
		} catch(e) {
				// if filter errors because there are no elements for the
				// selected genre, let it continue, and don't block
				if(App.debug) console.log(e);

				// if error, make API call
				overlay.start();
				retrieve_and_build_artists(function() {
					overlay.finish();
				});

		}

		var container_offset = el.offset().top,
				body_scroll = $('body').scrollTop() || $(document).scrollTop(),
				items = $('.item:not(.isotope-hidden)');
		
		// if the list isnt filled out completely, make an API
		// call to fill it out. This only has to be done once,
		// so once it is called, this should never execute ever again.
		if(items.length < 12) {

			retrieve_and_build_artists({
				before: overlay.start
			}, function() {
				overlay.finish();
			});

			if(container_offset < body_scroll) {
				window.scrollTo(0, container_offset - 70);
			}
		}

	}; // end isotope.filter

	var previous_filter;

	function retrieve_and_build_artists(obj, fn) {

		if(_.isFunction(obj)) {
			fn = obj;
		}

		obj.before = obj.before || function() {};

		var filters = isotope.filters();

		_.each(filters, function(filter, idx) {

			// to avoid repeat requests like one caused above,
			// where it checks for less than 12 items in order to populate
			if(previous_filter === filter) {
				return;
			}
			previous_filter = filter;
			// reset the filter after 500 milliseconds, as there may be requests
			// per infinite scroll 
			setTimeout(function() {
				previous_filter = undefined;
			}, 1000);

			if(_.contains(App.API.filters_no_more_artists, filter)) {
				isotope.filter(App.container_el, isotope.filters());
			}

			obj.before.call(overlay);

			if(filter === '*') {
				API.get_all_artists(function(data) {
					isotope.utils.build_loaded_items(App.container_el, data);

					fn();
				});
			} else {
				var flag = 1,
						limit = 12;

				// if the filter isn't in the list, add it to the list
				if(!_.contains(API.filters_called, filter)) {
					API.filters_called.push(filter);
				} else {
				// otherwise set a flag for that specific filter to 0
					flag = 0;
				}

				// divide the limit up as evenly as possible between all selected filters
				if(isotope.filters().length > 1) {
					limit = Math.floor(limit / isotope.filters().length);
				}

				API.get_artist_by_genre({
					genre: filter,
					flag: flag,
					limit: limit
				}, function(data) {
					// expand the data with the filters length
					data.filters_length = filters.length;
					isotope.utils.build_loaded_items(App.container_el, data);

					fn();
				});

			}
		});

	}

	isotope.utils = {};

	//times called for build_loaded_items
	var times = 0;

	isotope.utils.build_loaded_items = function(el, data) {

		if(!data.success) {
			times++;

			if(!_.contains(App.API.filters_no_more_artists, data.genre)) {
				App.API.filters_no_more_artists.push(data.genre);
			}

			if(times === data.filters_length) {
				var diff = _.difference(isotope.filters(), App.API.filters_no_more_artists);
				
				// if there is no difference betweent the filters, then they are
				// all success: false; which means we are safe to display the load_error
				if(!diff.length) {
					load_more_error('No more artists available for this genre..', function() {
						el.infinitescroll('pause');
					});
				}

				//reset times
				times = 0;
			}

			overlay.finish();
			return;
		}

		isotope.position(data, function(positioned_artitsts) {

			data.artist = positioned_artitsts;

		 	var html = maketemplate('artists.ejs', data),
					parsed = $.parseHTML(html),
					// get just the div's from the html
					// it has a bunch of spaces/text we want to ignore
					items = _.where(parsed, { nodeName: 'DIV' });

				_.each(items, function(item) {
					item = $(item).addClass('invisible');
					el
						.append(item)
						.isotope('appended', item, function() {

							isotope.filter(el, isotope.filters());
							el.isotope('reLayout');

							_.delay(isotope.utils.remove_invisiblity, 500);

							if(data.callback) {
								data.callback();
							}
						});
				});

			}); // isotope.position end

	}; // end isotope.build_loaded_items

	isotope.utils.remove_invisiblity = function(show_loading) {
		$('.item.invisible').removeClass('invisible');
	}; // end isotope.utils.remove_invisibility

	return {
		init: isotope.init,
		position: isotope.position,
		filter: isotope.filter,
		filters_init: isotope.filters_init
	};

});