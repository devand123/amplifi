define(['app', 'utils/overlay'], function(App, overlay) {
	
	App.API = {};

	App.API.filters_called = [];
	App.API.filters_no_more_artists = [];

	App.API.root = '/api';

	App.API.get = function(obj) {
		$.ajax({
			url: App.API.root + obj.url,
			type: 'GET',
			dataType: 'json',
			success: function(data) {
				obj.callback.call(this, data);
			},
			error: function(err, err2, err3) {
				overlay.start('error');
				if(App.debug) {
					console.log(err, err2, err3);
				}
			}
		})
	}; // end App.API.get()
	
	 App.API.get_genres = function(fn) {
	 	App.API.get({
	 		url: '/genre',
	 		callback: fn
	 	});
	 }; // end App.API.genres

	 App.API.get_artist_by_genre = function(obj, fn) {
	 	if(_.isFunction(obj)) {
	 		fn = obj;
	 	}
	 	obj.limit = obj.limit || 12;
	 	obj.flag = obj.flag || 0;

	 	App.API.get({
	 		url: '/artist/' + obj.genre + '/' + obj.limit + '/' + obj.flag,
	 		callback: fn
	 	});

	 }; // end App.API.get_artist_by_genre

	 App.API.get_all_artists = function(obj, fn) {
	 	// if the object is ommitted, reassign the first arg
	 	// as the callback
	 	if(_.isFunction(obj)) {
	 		fn = obj;
	 	}
	 	obj = obj || {};
	 	obj.limit = obj.limit || 12;
	 	obj.flag = obj.flag || 0;

	 	App.API.get({
	 		url: '/all_artists/' + obj.limit + '/' + obj.flag,
	 		callback: fn
	 	});
	 }; // end App.API.get_all_artists

	return {
		filters_called: App.API.filters_called,
		get: App.API.get,
		get_genres: App.API.get_genres,
		get_artist_by_genre: App.API.get_artist_by_genre,
		get_all_artists: App.API.get_all_artists
	};

});