// globally define these important libraries
// every file that includes 'app' will include whatever is here
define(['jquery', 'underscore', 'ejs'], function ($, _, EJS) {
    'use strict';

    var App = {};
    
    App.debug = false;

    App.init = function(fn) {
  		window.EJS = window.EJS || EJS;
  		$(function() {
  			fn();
  		});
    };

    return App;
});